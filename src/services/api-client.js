import axios from "axios";

const apiClient = axios.create({
    baseURL: process?.env?.VUE_APP_API_URL,
    timeout: 5000,
});

apiClient.interceptors.request.use(async (config) => {
    const accessToken = localStorage
        .getItem('token');

    if (accessToken) {
        config.headers.Authorization = `Bearer ${accessToken}`;
    }

    return config;
});

apiClient.interceptors.response.use((response) => {
    return response
}, async function (error) {
    if (error?.response?.status === 401) {
        localStorage.removeItem('token');
        window.location = "/login?redirect=" + `${window.location.protocol}//${window.location.hostname}${window.location.pathname}`;
    }
    return Promise.reject(error);
});


export default apiClient;