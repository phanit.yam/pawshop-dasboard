export const getEnv = (name, fallback = '') => {
  return process.env?.[name] || (import.meta.env[name]) || fallback;
}

export const API_URL = getEnv('VUE_APP_API_URL', '');
export const SYN_URL = getEnv('VUE_APP_SYNFUSION_KEY', '');