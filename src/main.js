import { createApp } from 'vue'
import App from './App.vue'
import { router } from './routes'
import '@/assets/base-style.scss';
import './index.scss';
import 'flowbite';
import { library } from '@fortawesome/fontawesome-svg-core';
import { FontAwesomeIcon } from '@fortawesome/vue-fontawesome';
import { faUserSecret, faChartPie,faAddressBook,faBook,faListAlt,faFileSignature,faCalendar,faFileContract, faBars,faAddressCard, faCaretDown,faChartColumn,faHandHoldingDollar, faList, faUserGroup,faUsers, faStore, faCartArrowDown, faCreditCard, faMoneyCheckDollar, faMoneyBills, faGear, faDrawPolygon, faDollarSign, faUserAlt, faLock, faPaperclip, faSquareCheck, faSquare, faCheck, faAngleDown, faPenToSquare, faTrash, faCartFlatbed, faBuilding, faUserCheck, faUserXmark, faSackDollar, faPhone, faUser, faCalculator, faWrench, faArrowTrendUp, faClock, faCircleCheck, faCircle, faBriefcase, faCircleXmark, faTrashCan, faUserTag, faClose, faDiagramPredecessor, faPlus, faSave } from '@fortawesome/free-solid-svg-icons'
import { store } from './store/index';
import VueLoading from 'vue-loading-overlay';
import 'vue-loading-overlay/dist/vue-loading.css';
import { faOpencart } from '@fortawesome/free-brands-svg-icons';
import ElementPlus from 'element-plus';
import dayjs from 'dayjs';
// import '@/helpers/vee-validate';


library.add(faUserSecret,
    faChartPie, faBars,
    faCaretDown,
    faChartColumn,
    faList,
    faStore,
    faUserGroup,
    faCartArrowDown,
    faCreditCard,
    faMoneyCheckDollar,
    faMoneyBills,
    faGear,
    faDrawPolygon,
    faDollarSign,
    faUserAlt, faLock,
    faPaperclip,
    faSquareCheck,
    faSquare,
    faCheck,
    faOpencart,
    faAngleDown,
    faPenToSquare,
    faTrash,
    faCartFlatbed,
    faBuilding,
    faUserCheck,
    faUserXmark,
    faSackDollar,
    faPhone,
    faUser,
    faUsers,
    faCalculator,
    faUserXmark,
    faWrench,
    faArrowTrendUp,
    faClock,
    faCircleCheck,
    faCircle,
    faBriefcase,
    faCircleXmark,
    faTrashCan,
    faUserTag,
    faCircleXmark,
    faClose,
    faDiagramPredecessor,
    faPenToSquare,
    faHandHoldingDollar,
    faAddressCard,
    faAddressBook,
    faFileSignature,
    faPlus,
    faFileContract,
    faCalendar,
    faListAlt,
    faSave,
    faBook
  
    
)

const app = createApp(App)
app.use(router)
app.use(store)
app.use(VueLoading);
app.use(ElementPlus, { size: 'large' });
app.config.globalProperties.$dayjs = dayjs;
app.directive('click-outside', {
    mounted(el, binding, vnode) {

        el.clickOutsideEvent = function (event) {
            if (!(el === event.target || el.contains(event.target))) {
                binding.value(event, el || vnode);
            }
        };
        document.body.addEventListener('click', el.clickOutsideEvent);
    },
    unmounted(el) {
        document.body.removeEventListener('click', el.clickOutsideEvent);
    }
});
app.config.globalProperties.$filters = {
    currencyUSD(value) {
        if (typeof value !== "number") {
            return value;
        }
        var formatter = new Intl.NumberFormat('en-US', {
            style: 'currency',
            currency: 'USD'
        });
        return formatter.format(value);
    }
}
app.component('fa-icon', FontAwesomeIcon)
app.mount('#app')
