const requireModule = require.context('./modules/', true, /\.(js)$/i);
const modules = {};

requireModule.keys().forEach(filename => {
    const moduleName = filename
        .replace(/(\.\/|\.js)/g, '')
        .replace(/^\w/, c => c.toUpperCase());

    modules[moduleName] = requireModule(filename).default || requireModule(filename);
});

export default modules;