
import { SET_USER_ROLE, SET_LOADING, SET_TOTAL_RECORD } from "../constant";


const state = {
    isLoading: false,
    items: [
        {
            id:1,
            fullName:'Tang Pich',
            username: 'Tang.Pich',
            email:'pich.tang@lhp.com',
            phoneNumber:'012989898',
            position: 'Admin',
            departmentId: 'Admin',
            telegramId:'012989898',
            assignUserRole: 'Admin',
            
        },
        {
            id:2,
            fullName:'Tang Pich',
            username: 'Tang.Pich',
            email:'pich.tang@lhp.com',
            phoneNumber:'012989898',
            position: 'Admin',
            departmentId: 'Admin',
            telegramId:'012989898',
            assignUserRole: 'Admin',
        },
        {
            id:3,
            fullName:'Tang Pich',
            username: 'Tang.Pich',
            email:'pich.tang@lhp.com',
            phoneNumber:'012989898',
            position: 'Admin',
            departmentId: 'Admin',
            telegramId:'012989898',
            assignUserRole: 'Admin',
        },
        {
            id:4,
            fullName:'Tang Pich',
            username: 'Tang.Pich',
            email:'pich.tang@lhp.com',
            phoneNumber:'012989898',
            position: 'Admin',
            departmentId: 'Admin',
            telegramId:'012989898',
            assignUserRole: 'Admin',
        },
    ],
    totalRecord: 0
}

const actions = {
}

const mutations = {
    [SET_USER_ROLE](state, data) {
        state.items = data;
    },
    [SET_LOADING](state, data) {
        state.isLoading = data;
    },
    [SET_TOTAL_RECORD](state, data) {
        state.totalRecord = data;
    }
};

const getters = {
    items: (state) => state.items,
    isLoading: (state) => state.isLoading,
    totalRecord: (state) => state.totalRecord
}

export default {
    namespaced: true,
    state,
    getters,
    actions,
    mutations
};