import apiClient from "@/services/api-client";
import axios from "axios";
import { ElNotification } from "element-plus";

const SET_TOKEN = 'SET_TOKEN';
const AUTH_LOGOUT = 'AUTH_LOGOUT';
const SET_LOADING = 'SET_LOADING';

const state = {
    token: localStorage.getItem('token')
}

const getters = {
    isLoggedIn: state => !!state.token,
}

const mutations = {
    [SET_TOKEN](state, token) {
        state.token = token;
    },
    [AUTH_LOGOUT](state) {
        localStorage.removeItem('token');
        state.token = "";
    }
}

const actions = {
    async onLogin({ commit }, data = {}) {
        commit(SET_LOADING, true, { root: true });
        apiClient.post('/auth/authenticate', data)
            .then(({ data }) => {
                const { token, users } = data;
                const urlParams = new URLSearchParams(window.location.search);
                if (token) {
                    localStorage.setItem('token', token);
                    localStorage.setItem('users', users)
                    axios.defaults.headers.common = { 'Authorization': `bearer ${token}` }
                    commit(SET_TOKEN, token);

                    window.location = urlParams.get("redirect") || "/";
                }
                // commit(SET_LOADING, false, { root: true });
            })
            .catch(error => {
                commit(SET_LOADING, false, { root: true });
                ElNotification({
                    title: 'Login Failed!',
                    message: "The identity is not valid. Please check your username and password again.",
                    type: 'error',
                })
                console.log(error);
            });
    },
    onLogout({ commit }) {
        commit(AUTH_LOGOUT);
        if (window.location.pathname == "/login") return;
        window.location = "/login?redirect=" + `${window.location.href}`;
    }
}

export default {
    namespaced: true,
    state,
    mutations,
    actions,
    getters
};
