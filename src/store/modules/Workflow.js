import apiClient from "@/services/api-client";

const actions = {
    /* eslint-disable */
    async onSubmitWorkflow({ commit }, _data) {
        try {
            await apiClient.post('/workflows/submit', _data);
        } catch (e) {
            console.log(e);
        }
    },
}

export default {
    namespaced: true,
    actions
};