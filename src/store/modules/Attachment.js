import apiClient from "@/services/api-client";
import { SET_LOADING } from "../constant";
import { ElNotification } from 'element-plus';

const setFiles = "setFiles";
const setFileBase64 = "setFileBase64";

const state = {
    isLoading: false,
    files: [],
    fileBase64: ""
}

// const blobToBase64 = (blob) => {
//     return new Promise((resolve) => {
//         const reader = new FileReader();
//         reader.onloadend = () => resolve(reader.result);
//         reader.readAsDataURL(blob);
//     });
// }

const actions = {
    async fetchFileView({ commit }, uuid) {
        try {
            commit(SET_LOADING, true);
            const { data } = await apiClient.get(`/attachments/image/${uuid}`);

            commit("setFileBase64", data);
        } catch (e) {
            console.error(e.message);
        } finally {
            commit(SET_LOADING, false);
        }
    },
    async deleteFile({ commit }, uuid) {
        try {
            commit(SET_LOADING, true);
            await apiClient.post(`/attachments/delete`, { fileUuid: uuid });
        } catch (e) {
            console.error(e.message);
        } finally {
            commit(SET_LOADING, false);
        }
    },
    async fetchFiles({ commit }, { ref, id }) {
        try {
            commit(SET_LOADING, true);
            const { data } = await apiClient.get(`/attachments/${ref}/${id}`);
            commit(setFiles, data || []);
        } catch (e) {
            console.error(e.message);
        } finally {
            commit(SET_LOADING, false);
        }
    },
    async uploadFile({ commit, dispatch }, { file, ref, id }) {
        try {
            commit(SET_LOADING, true);
            const formData = new FormData();
            formData.append("file", file);
            const headers = {
                'Content-Type': 'multipart/form-data'
            }

            await apiClient.post(`/attachments/upload/${ref}/${id}`, formData, {
                headers
            });
            await dispatch("fetchFiles", { ref, id });
            commit(SET_LOADING, false);
        } catch (e) {
            console.error(e.message);
            ElNotification({
                title: 'Error',
                message: e.message,
                type: 'error',
            })
        } finally {
            commit(SET_LOADING, false);
        }
    }
}

const mutations = {
    [SET_LOADING](state, data) {
        state.isLoading = data;
    },
    [setFiles](state, data) {
        state.files = data;
    },
    [setFileBase64](state, fileBase64) {
        state.fileBase64 = fileBase64;
    }
};

export default {
    namespaced: true,
    state,
    actions,
    mutations
};