
import { SET_LOANAPPVALUATION, SET_LOADING, SET_TOTAL_RECORD } from "../constant";


const state = {
    isLoading: false,
    items: [
        {
            id:1,
            no:'LHP:0001/23',
            nameKh:'តាំង ពេជ្រ',
            code: 'LHP-0001',
            abbreviation:'Pawnshop',
            address:'Phnom Penh',
            startDate: '2023-02-14',
            
        },
        {
            id:2,
            no:'LHP:0002/23',
            nameKh:'មន្នី ពេជ្រ',
            code: 'LHP-0002',
            abbreviation:'Pawnshop',
            address:'Phnom Penh',
            startDate: '2023-02-14',
        },
        {
            id:3,
            no:'LHP:0003/23',
            nameKh:'រស្មី ពេជ្រ',
            code: 'LHP-0003',
            abbreviation:'Pawnshop',
            address:'Phnom Penh',
            startDate: '2023-02-14',
        },
        {
            id:4,
            no:'LHP:0004/23',
            nameKh:'កោះ ពេជ្រ',
            code: 'LHP-0004',
            abbreviation:'Pawnshop',
            address:'Phnom Penh',
            startDate: '2023-02-14',
        },
    ],
    totalRecord: 0
}

const actions = {
}

const mutations = {
    [SET_LOANAPPVALUATION](state, data) {
        state.items = data;
    },
    [SET_LOADING](state, data) {
        state.isLoading = data;
    },
    [SET_TOTAL_RECORD](state, data) {
        state.totalRecord = data;
    }
};

const getters = {
    items: (state) => state.items,
    isLoading: (state) => state.isLoading,
    totalRecord: (state) => state.totalRecord
}

export default {
    namespaced: true,
    state,
    getters,
    actions,
    mutations
};