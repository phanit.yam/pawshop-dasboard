
import { SET_CUSTOMERPROFILE, SET_LOADING, SET_TOTAL_RECORD } from "../constant";


const state = {
    isLoading: false,
    items: [
        {
            id:1,
            nameKh:'តាំង ពេជ្រ',
            nameEn: 'Tang Pich',
            gender:'Male',
            nationality:'Cambodian',
            dob: '2023-02-14',
            idNo: '01234567',
            phoneContract:'012989898',
            
        },
        {
            id:2,
            nameKh:'តាំង ពេជ្រ',
            nameEn: 'Tang Pich',
            gender:'Male',
            nationality:'Cambodian',
            dob: '2023-02-14',
            idNo: '01234567',
            phoneContract:'012989898',
        },
        {
            id:3,
            nameKh:'តាំង ពេជ្រ',
            nameEn: 'Tang Pich',
            gender:'Male',
            nationality:'Cambodian',
            dob: '2023-02-14',
            idNo: '01234567',
            phoneContract:'012989898',
        },
        {
            id:4,
            nameKh:'តាំង ពេជ្រ',
            nameEn: 'Tang Pich',
            gender:'Male',
            nationality:'Cambodian',
            dob: '2023-02-14',
            idNo: '01234567',
            phoneContract:'012989898',
        },
    ],
    totalRecord: 0
}

const actions = {
}

const mutations = {
    [SET_CUSTOMERPROFILE](state, data) {
        state.items = data;
    },
    [SET_LOADING](state, data) {
        state.isLoading = data;
    },
    [SET_TOTAL_RECORD](state, data) {
        state.totalRecord = data;
    }
};

const getters = {
    items: (state) => state.items,
    isLoading: (state) => state.isLoading,
    totalRecord: (state) => state.totalRecord
}

export default {
    namespaced: true,
    state,
    getters,
    actions,
    mutations
};