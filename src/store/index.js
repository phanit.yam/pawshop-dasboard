// store.js
import apiClient from '@/services/api-client';
import { ElNotification } from 'element-plus';
import { createStore } from 'vuex'
import modules from './module';

const SET_LOADING = 'SET_LOADING';
const SET_FULL_PAGE = 'SET_FULL_PAGE';
const SET_IS_OPEN = 'SET_IS_OPEN';
const SET_IS_OVER = 'SET_IS_OVER';


export const store = createStore({
    state: {
        isLoading: false,
        isFullPage: true,
        isOpen: true,
        isOver: true
    },
    actions: {
        async deleteItem({ commit }, { namespace, id, notLoading }) {
            try {
                if (!notLoading) {
                    commit(SET_LOADING, true);
                }

                await apiClient.put(`/${namespace}/update/status`, { id, status: 0 });
            } catch (e) {
                ElNotification({
                    title: 'Error',
                    message: "Internal Server Error. Please try again!",
                    type: 'error',
                })
                throw e?.message;
            } finally {
                if (!notLoading) {
                    commit(SET_LOADING, false);
                }
            }
        },
        async deleteItemV1({ commit }, { namespace, id, notLoading }) {
            try {
                if (!notLoading) {
                    commit(SET_LOADING, true);
                }

                await apiClient.put(`/${namespace}/change-status`, { id, status: 0 });
            } catch (e) {
                ElNotification({
                    title: 'Error',
                    message: "Internal Server Error. Please try again!",
                    type: 'error',
                })
                throw e?.message;
            } finally {
                if (!notLoading) {
                    commit(SET_LOADING, false);
                }
            }
        },
        async deleteItemV2({ commit }, { namespace, id, notLoading }) {
            try {
                if (!notLoading) {
                    commit(SET_LOADING, true);
                }

                await apiClient.put(`/${namespace}/update/status`, { id, isActive: 0 });
            } catch (e) {
                ElNotification({
                    title: 'Error',
                    message: "Internal Server Error. Please try again!",
                    type: 'error',
                })
                throw e?.message;
            } finally {
                if (!notLoading) {
                    commit(SET_LOADING, false);
                }
            }
        },
    },
    mutations: {
        [SET_LOADING](state, val) {
            state.isLoading = val;
        },
        [SET_FULL_PAGE](state, val) {
            state.isFullPage = val;
        },
        [SET_IS_OPEN](state, val) {
            state.isOpen = val;
        },
        [SET_IS_OVER](state, val) {
            state.isOpen = val;
        }
    },
    modules
})


