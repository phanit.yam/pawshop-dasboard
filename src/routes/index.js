import VDashboard from '@/pages/VDashboard';
import VLogin from '@/pages/VLogin';
import VBrandInfo from "@/pages/BrandInfo/VBrandInfo";
import VCustomerProfile from '@/pages/CustomerProfile/VCustomerProfile';
import VCustomerEvaluation from '@/pages/CustomerEvaluation/VCustomerEvaluation';
import VLoanAppValuation from '@/pages/LoanAppValuation/VLoanAppValuation';
import VLoanAgreement from '@/pages/LoanAgreement/VLoanAgreement';
import VMortgetAgreement from '@/pages/MortgetAgreement/VMortgetAgreement';
import VReceivingCollateral from '@/pages/ReceivingCollateral/VReceivingCollateral';
import VBlockCollateral from '@/pages/BlockCollateral/VBlockCollateral';
import VDisbursementReguest from '@/pages/DisbursementReguest/VDisbursementReguest';
import VDisbursementVoucher from '@/pages/DisbursementVoucher/VDisbursementVoucher';
import VCollection from '@/pages/Collection/VCollection';
import VUser from '@/pages/User/VUser';
import VUserRole from '@/pages/UserRole/VUserRole';
import VReport from '@/pages/Report/VReport';





import VBrandInfoAdd from "@/pages/BrandInfo/VBrandInfoAdd";
import VCustomerProfileAdd from "@/pages/CustomerProfile/VCustomerProfileAdd";
import VCustomerEvaluationAdd from "@/pages/CustomerEvaluation/VCustomerEvaluationAdd";
import VLoanAppValuationAdd from "@/pages/LoanAppValuation/VLoanAppValuationAdd";
import VLoanAgreementAdd from '@/pages/LoanAgreement/VLoanAgreementAdd';
import VMortgetAgreementAdd from '@/pages/MortgetAgreement/VMortgetAgreementAdd';
import VReceivingCollateralAdd from '@/pages/ReceivingCollateral/VReceivingCollateralAdd';
import VBlockCollateralAdd from '@/pages/BlockCollateral/VBlockCollateralAdd';
import VDisbursementReguestAdd from '@/pages/DisbursementReguest/VDisbursementReguestAdd';
import VDisbursementVoucherAdd from '@/pages/DisbursementVoucher/VDisbursementVoucherAdd';
import VCollectionAdd from '@/pages/Collection/VCollectionAdd';
import VUserAdd from '@/pages/User/VUserAdd';
import VUserRoleAdd from '@/pages/UserRole/VUserRoleAdd';

import VBrandInfoEdit from '@/pages/BrandInfo/VBrandInfoEdit';
import VCustomerProfileEdit from '@/pages/CustomerProfile/VCustomerProfileEdit';
import VCustomerEvaluationEdit from '@/pages/CustomerEvaluation/VCustomerEvaluationEdit';
import VLoanAppValuationEdit from '@/pages/LoanAppValuation/VLoanAppValuationEdit';
import VLoanAgreementEdit from '@/pages/LoanAgreement/VLoanAgreementEdit';
import VMortgetAgreementEdit from '@/pages/MortgetAgreement/VMortgetAgreementEdit';
import VReceivingCollateralEdit from '@/pages/ReceivingCollateral/VReceivingCollateralEdit';
import VBlockCollateralEdit from '@/pages/BlockCollateral/VBlockCollateralEdit';
import VDisbursementReguestEdit from '@/pages/DisbursementReguest/VDisbursementReguestEdit';
import VDisbursementVoucherEdit from '@/pages/DisbursementVoucher/VDisbursementVoucherEdit';
import VCollectionEdit from '@/pages/Collection/VCollectionEdit';
import VUserEdit from '@/pages/User/VUserEdit';
import VUserRoleEdit from '@/pages/UserRole/VUserRoleEdit';

import { store } from "@/store";
import { createRouter, createWebHistory } from 'vue-router';


const isLoggined = async (to, from, next) => {
    const isWaiting = await store.getters["Auth/isLoggedIn"];
    if (isWaiting) {
        next();
        return;
    }
    next("/login");
};

const isNotLoggined = async (to, from, next) => {
    const isWaiting = await store.getters["Auth/isLoggedIn"];
    if (!isWaiting) {
        next();
        return;
    }
    next("/");
    return;
};

const routes = [
    {
        path: '/', component: VDashboard, beforeEnter: isLoggined,
    },
    { path: '/login', component: VLogin, beforeEnter: isNotLoggined },
    {
        path: "/brand-info",
        component: VBrandInfo,
        beforeEnter: isLoggined,
    },
    {
        path: "/brand-info/add",
        component: VBrandInfoAdd,
        beforeEnter: isLoggined,
    },
    {
        path: "/brand-info/edit/:id",
        component: VBrandInfoEdit,
        beforeEnter: isLoggined,
    },
    {
        path:'/customer',
        meta: {
            requiresAuth: true,
            allow: 'customer'
        },
        children:[
            {
                path: "/customer-profile",
                component:VCustomerProfile,
                beforeEnter: isLoggined,
            },
            {
                path: "/customer-profile/add",
                component:VCustomerProfileAdd,
                beforeEnter: isLoggined,
            },
            
            {
                path: "/customer-profile/edit/:id",
                component: VCustomerProfileEdit,
                beforeEnter: isLoggined,
            },
            {
                path: "/customer-evaluation",
                component:VCustomerEvaluation,
                beforeEnter: isLoggined,
            },
            {
                path: "/customer-evaluation/add",
                component:VCustomerEvaluationAdd,
                beforeEnter: isLoggined,
            },
            
            {
                path: "/customer-evaluation/edit/:id",
                component: VCustomerEvaluationEdit,
                beforeEnter: isLoggined,
            },

        ]
    },

    {
        path: '/loan',
        meta: {
            requiresAuth: true,
            allow: 'loan'
        },
        children: [
            {
                path: 'app-valuation',
                component: VLoanAppValuation,
                
            },
            {
                path: 'app-valuation/add',
                component: VLoanAppValuationAdd,
               
            },
            {
                path: "app-valuation/edit/:id",
                component: VLoanAppValuationEdit,
               
            },
            {
                path: "agreement",
                component: VLoanAgreement,
               
            },
            {
                path: "agreement/add",
                component: VLoanAgreementAdd,
               
            },
            {
                path: 'agreement/edit/:id',
                component: VLoanAgreementEdit,
               
            }
        ]
    },
    {
        path: "/mortget-agreement",
        component: VMortgetAgreement,
        beforeEnter: isLoggined,
    },
    {
        path: "/mortget-agreement/add",
        component: VMortgetAgreementAdd,
        beforeEnter: isLoggined,
    },
    {
        path: "/mortget-agreement/edit/:id",
        component: VMortgetAgreementEdit,
        beforeEnter: isLoggined,
    },
    {
        path: '/collateral',
        meta: {
            requiresAuth: true,
            allow: 'collateral'
        },
        children: [
            {
                path: 'receiving-collateral',
                component: VReceivingCollateral,
                
            },
            {
                path: 'receiving-collateral/add',
                component: VReceivingCollateralAdd,
                
            },
            {
                path: 'receiving-collateral/edit/:id',
                component: VReceivingCollateralEdit,
               
            },
            {
                path: 'block-collateral',
                component: VBlockCollateral,
                
            },
            {
                path: 'block-collateral/add',
                component: VBlockCollateralAdd,
               
            },
            {
                path: 'block-collateral/edit/:id',
                component: VBlockCollateralEdit,
                
            },
        ]
    },

    {
        path: '/disbursement',
        meta: {
            requiresAuth: true,
            allow: 'disbursement'
        },
        children: [
            {
                path: 'reguest',
                component: VDisbursementReguest,
               
            },
            {
                path: 'reguest/add',
                component: VDisbursementReguestAdd,
               
            },
            {
                path: 'reguest/edit/:id',
                component: VDisbursementReguestEdit,
                
            },
            {
                path: 'voucher',
                component: VDisbursementVoucher,
               
            },
            {
                path: 'voucher/add',
                component: VDisbursementVoucherAdd,
               
            },
            {
                path: 'voucher/edit/:id',
                component: VDisbursementVoucherEdit,
               
            },
        ]
    },
    {
        path: "/collection",
        component: VCollection,
        beforeEnter: isLoggined,
    },
    {
        path: "/collection/add",
        component: VCollectionAdd,
        beforeEnter: isLoggined,
    },
    {
        path: "/collection/edit/:id",
        component: VCollectionEdit,
        beforeEnter: isLoggined,
    },


    {
        path: '/setting',
        meta: {
            requiresAuth: true,
            allow: 'setting'
        },
        children: [
            {
                path: 'user',
                component: VUser,
               
            },
            {
                path: 'user/add',
                component: VUserAdd,
                
            },
            {
                path: 'user/edit/:id',
                component: VUserEdit,
               
            },
            {
                path: 'user-role',
                component: VUserRole,
               
            },
            {
                path: 'user-role/add',
                component: VUserRoleAdd,
               
            },
            {
                path: 'user-role/edit/:id',
                component: VUserRoleEdit,
                
            },
        ]
    },
    {
        path: '/report',
        component: VReport, meta: {
            allow: "report"
        }
    },


]

export const router = createRouter({
    history: createWebHistory(),
    routes,
    linkActiveClass: 'bg-primary-70 border-l-4 border-secondary',
})