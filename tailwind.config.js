/** @type {import('tailwindcss').Config} */
module.exports = {
  content: [
    "./index.html",
    "./src/**/*.{vue,js,ts,jsx,tsx}",
    "./node_modules/flowbite/**/*.js"
  ],
  theme: {
    extend: {
      colors: {
        transparent: 'transparent',
        'primary': '#025471',
        'primary-70': '#207FA0B2',
        'primary-80': '#0A6D90',
        'secondary': '#E2B124',
        'custom-mm': '#F3F3F31A',
        'primary-20': '#CBCBCB80',
        'ap': '#F7F7FE',
        'dash-card-1': '#E2B1244D',
        'dash-card-2': '#0AA1DD4D',
        'dash-card-3': '#2FA0394D',
        'dash-card-4': '#5A3E9E4D',
        'ap-grey-5': '#fbfaf0',
        'success': '#2FA039',
        'danger': '#EA1313',

        'primary-30': '#0670B8',
        'secondary-20': '#F15412',
        'meduim-10': '#FF9C47',


      },
      fontFamily: {
        mm: ["MontserratMedium", 'KHMoul'],
        mn: ['MontserratN', 'KHSR'],
        mc: ['MontserratN', 'KHMetal'],
        ms:['ENDefoe'],
      },
    },
  },
  plugins: [],
}
